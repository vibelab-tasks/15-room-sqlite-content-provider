package com.example.contactnotes

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.contactnotes.models.Contact
import com.example.contactnotes.models.ContactViewModel
import com.example.contactnotes.models.ContactViewModelFactory

class MainActivity : AppCompatActivity() {

    private val contactViewModel: ContactViewModel by viewModels {
        ContactViewModelFactory((application as ContactsApplication).repository)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview)
        val adapter = ContactListAdapter(ContactListAdapter.OnClickListener { contact ->
            val intent = Intent(this, EditDescriptionActivity::class.java).apply {
                putExtra("phone_number", contact.phoneNumber)
                putExtra("name", contact.name)
            }
            startActivityForResult(intent, 1)
        })
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        val contacts = getContacts()
        val roomContacts = contactViewModel.allContacts.value


        if (roomContacts == null)
            contacts.forEach{contactViewModel.insert(it)}
        else {
            for (contact in contacts)
                if (roomContacts.indexOfFirst { it.phoneNumber == contact.phoneNumber } == -1) {
                    contactViewModel.insert(contact)
                }

            for (rContact in roomContacts)
                if (contacts.indexOfFirst { it.phoneNumber == rContact.phoneNumber } == -1)
                    contactViewModel.deleteByPhoneNumber(rContact.phoneNumber)
        }

        contactViewModel.allContacts.observe(this, Observer { contacts ->
            contacts?.let { adapter.submitList(it) }
        })

    }

    fun getContacts() : MutableList<Contact> {
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_CONTACTS), 0)
        }

        val contacts: MutableList<Contact> = ArrayList()
        val cursor = contentResolver.query(
            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
            null, null, null, null)
        if (cursor != null && cursor.moveToFirst()) {
            do {
                val name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
                val phoneNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                contacts.add(Contact(phoneNumber, name, ""))
            } while (cursor.moveToNext())

            cursor.close()
        }
        return contacts
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && data?.getStringExtra("phone_number") != null) {
            val phoneNumber = data.getStringExtra("phone_number")
            val description = data.getStringExtra("new_description")

            val i = contactViewModel.allContacts.value!!.indexOfFirst { it.phoneNumber == phoneNumber }
            contactViewModel.insert(Contact(
                phoneNumber = phoneNumber!!,
                description = description ?: "",
                name = contactViewModel.allContacts.value!![i].name))
        } else {
            Toast.makeText(
                applicationContext,
                "Description not saved",
                Toast.LENGTH_LONG
            ).show()
        }
    }
}