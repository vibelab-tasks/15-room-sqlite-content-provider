package com.example.contactnotes

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class EditDescriptionActivity : AppCompatActivity() {

    private lateinit var etEditDescription: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_description)
        etEditDescription = findViewById(R.id.etEditDescription)
        findViewById<TextView>(R.id.tvContactName).text = intent.getStringExtra("name")

        val button = findViewById<Button>(R.id.btnSaveDescription)
        button.setOnClickListener {
            val replyIntent = Intent()
            if (TextUtils.isEmpty(etEditDescription.text)) {
                setResult(Activity.RESULT_CANCELED, replyIntent)
            } else {
                val description = etEditDescription.text.toString()
                replyIntent.putExtra("new_description", description)
                replyIntent.putExtra("phone_number", intent.getStringExtra("phone_number"))
                println("edit: " + intent.getStringExtra("phone_number"))
                setResult(Activity.RESULT_OK, replyIntent)
            }
            finish()
        }
    }

}