package com.example.contactnotes

import android.app.Application
import com.example.contactnotes.models.ContactRepository
import com.example.contactnotes.models.ContactRoomDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class ContactsApplication : Application() {
    val applicationScope = CoroutineScope(SupervisorJob())
    val database by lazy { ContactRoomDatabase.getDatabase(this, applicationScope) }
    val repository by lazy { ContactRepository(database.contactDao()) }
}