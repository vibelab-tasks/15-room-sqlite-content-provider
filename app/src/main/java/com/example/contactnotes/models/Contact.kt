package com.example.contactnotes.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "contact_table")
data class Contact(
    @PrimaryKey @ColumnInfo(name = "phone_number") val phoneNumber: String,
    val name: String,
    val description: String) {
}