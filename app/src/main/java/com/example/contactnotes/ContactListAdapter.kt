package com.example.contactnotes

import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.contactnotes.models.Contact

class ContactListAdapter(private val onClickListener: OnClickListener) : ListAdapter<Contact, ContactListAdapter.ContactViewHolder>(ContactsComparator()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {
        return ContactViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        val current = getItem(position)
        holder.bind(current, onClickListener)
    }

    class OnClickListener(val clickListener: (contact: Contact) -> Unit) {
        fun onClick(contact: Contact) = clickListener(contact)
    }

    class ContactViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val tvNameLabel: TextView = itemView.findViewById(R.id.tvNameLabel)
        private val tvDescriptionLabel: TextView = itemView.findViewById(R.id.tvDescriptionLabel)
        private val tvPhoneNumberLabel: TextView = itemView.findViewById(R.id.tvPhoneNumberLabel)
        private val btnEdit: ImageButton = itemView.findViewById<ImageButton>(R.id.imageButton)

        fun bind(contact: Contact, clickListener: OnClickListener ) {
            tvNameLabel.text = contact.name
            tvDescriptionLabel.text = contact.description
            tvPhoneNumberLabel.text = contact.phoneNumber
            btnEdit.setOnClickListener{ clickListener.onClick(contact) }
        }

        companion object {
            fun create(parent: ViewGroup): ContactViewHolder {
                val view: View = LayoutInflater.from(parent.context)
                    .inflate(R.layout.recyclerview_item, parent, false)
                return ContactViewHolder(view)
            }
        }
    }

    class ContactsComparator : DiffUtil.ItemCallback<Contact>() {
        override fun areItemsTheSame(oldItem: Contact, newItem: Contact): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Contact, newItem: Contact): Boolean {
            return oldItem.name == newItem.name && oldItem.description == newItem.description
        }
    }
}